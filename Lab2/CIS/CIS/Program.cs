﻿namespace Animals
{
    // Базовий клас, від якого будуть наслідуватися інші класи
    public class Animal
    {
        public void Eat()
        {
            Console.WriteLine("The animal is eating.");
        }
    }

    // Абстрактний клас, який розширює функціонал базового класу
    public abstract class Mammal : Animal
    {
        public abstract void Roar();
    }

    // Інтерфейс, який вимагає реалізації методу в класах, які його реалізують
    public interface IMove
    {
        void Run();
    }

    // Клас, який реалізує абстрактний клас та інтерфейс
    public class Dog : Mammal, IMove
    {
        public override void Roar()
        {
            Console.WriteLine("The dog says 'RRRRRR'");
        }

        public void Run()
        {
            Console.WriteLine("The dog is running.");
        }
    }

    class Cat
    {
        public string Name { get; set; } // доступний з будь-якого місця
        private int Age { get; set; } // доступний тільки у самому класі
        protected string Color { get; set; } // доступний у самому класі та його нащадках

        public Cat(string name, int age, string color)
        {
            Name = name;
            Age = age;
            Color = color;
        }

        private void Sleep()
        {
            Console.WriteLine($"{Name} is sleeping!");
        }

        public void DisplayCatInfo()
        {
            this.Sleep();
        }

        public void Meow()
        {
            Console.WriteLine($"{Name} is meowing!");
        }

        protected void Eat()
        {
            Console.WriteLine($"{Name} is eating!");
        }
    }

    // Клас нащадок з доступом до protected методу батьківського класу
    class Kitten : Cat
    {
        public Kitten(string name, int age, string color) : base(name, age, color)
        {

        }

        public void Play()
        {
            Console.WriteLine($"{Name} is playing.");
            //this.Sleep();
        }
    }

    struct Fish
    {
        public string name;
        int age;

        void Swim()
        {
            Console.WriteLine("Bul Bul");
        }
    }

    interface IBird
    {
        void Sing();
    }

    class Bird
    {
        string name;
        int age;

        public Bird(string name, int age)
        {
            this.name = name;
            this.age = age;
        }

        void Fly()
        {
            Console.WriteLine($"{name} is flying.");
        }

        class MiniBird
        {
            public void Jump()
            {
                Console.WriteLine("MiniBird is jumping.");
            }
        }
    }

}

namespace Classes
{
    public class OuterClass
    {
        private int outerField = 10;

        protected class InnerClass
        {
            public void PrintOuterField(OuterClass outer)
            {
                Console.WriteLine($"Outer field value: {outer.outerField}");
            }
        }

        private InnerClass inner = new InnerClass();

        void Output()
        {
            Console.WriteLine(inner);
        }
    }
}

namespace Enums
{
    public enum Months
    {
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9,
        October = 10,
        November = 11,
        December = 12
    }

    public class Calendar
    {
        public void LogicOperators()
        {
            Months current = Months.April;

            // побітові оператори
            Console.WriteLine(Months.January | Months.February); // 01 | 10 => 11 => 3 => March
            Console.WriteLine(Months.March & Months.February); // 11 & 10 => 10 => 2 => February
            Console.WriteLine(Months.March ^ Months.April); // 011 ^ 100 => 111 => 7 => July

            // логічні оператори
            Console.WriteLine(Months.January == current || Months.April == current); // F or T => T
            Console.WriteLine(Months.January == current && Months.April == current); // F and T => F

        }

        public void ShowMonth()
        {
            Months currentMonth = Months.January;
            Console.WriteLine("The current month is: " + currentMonth);
        }
    }
}

namespace Inheritance
{
    interface IParentA
    {
        void FuncA();
    }

    interface IParentB
    {
        void FuncB();
    }

    class MyClass : IParentA, IParentB
    {
        public void FuncA()
        {
            Console.WriteLine("Method A");
        }

        public void FuncB()
        {
            Console.WriteLine("Method B");
        }
    }
}

namespace Overload
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public Person(string name)
        {
            Name = name;
        }

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public virtual void SayHello()
        {
            Console.WriteLine("Hello, my name is " + Name);
        }
    }

    public class Employee : Person
    {
        public int Salary { get; set; }

        public Employee(string name, int age, int salary) : base(name, age)
        {
            Salary = salary;
        }

        public Employee(string name, int salary) : this(name, 0, salary)
        {
        }

        public override void SayHello()
        {
            Console.WriteLine("Hello, my name is " + Name + " and I am working!");
        }
    }
}

namespace Initializations
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }
    }

    public class PersonStatic
    {
        public static int Count { get; private set; }

        static PersonStatic()
        {
            Count = 0;
        }

        public PersonStatic()
        {
            Count++;
        }
    }

    public class PersonDynamic
    {
        public string Name { get; set; } = "John";
        public int Age { get; set; } = 30;
    }

    public class PersonMethod
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public void Initialize(string name, int age)
        {
            Name = name;
            Age = age;
        }
    }
}

namespace OutRef
{
    class Params
    {
        public static void Divide(int dividend, int divisor, out int quotient, out int remainder)
        {
            quotient = dividend / divisor;
            remainder = dividend % divisor;
        }

        public static void Increment(ref int value)
        {
            value++;
        }

        public static void DivideNoOut(int dividend, int divisor, int quotient, int remainder)
        {
            quotient = dividend / divisor;
            remainder = dividend % divisor;
        }

        public static void IncrementNoRef(int value)
        {
            value++;
        }

        public static void DivideNoSense(int dividend, int divisor, out int quotient, out int remainder)
        {
            quotient = 3;
            remainder = 2;
        }

        public static void IncrementNoSense(ref int value)
        {
            value = 1;
        }
    }
}

namespace Lab2
{

    class Program
    {
        public static void Main()
        {
            int quotient = 0, remainder = 0;
            OutRef.Params.DivideNoSense(12, 4, out quotient, out remainder);
            Console.WriteLine($"Quotient: {quotient}, Remainder: {remainder}");

            int x = 10;
            OutRef.Params.IncrementNoSense(ref x);
            Console.WriteLine($"x = {x}");
        }
    }






    class Pinguin : Animals.IBird
    {
        public void Sing()
        {
            Console.WriteLine("I cant sign.");
        }
    }

}
