#include <iostream>
#include <chrono>

struct Arithmetic {
    static double Multiply(double a, double b) {
        return a * b;
    }

    static double Subtract(double a, double b) {
        return a - b;
    }

    static double Divide(double a, double b) {
        if (b == 0) {
            throw std::invalid_argument("Cannot divide by zero");
        }
        return a / b;
    }
};

struct CharOperations {
    static bool IsLetter(char c) {
        return isalpha(c);
    }

    static bool IsDigit(char c) {
        return isdigit(c);
    }

    static bool IsWhiteSpace(char c) {
        return isspace(c);
    }

    static bool IsPunctuation(char c) {
        return ispunct(c);
    }

    static bool IsSymbol(char c) {
        return isgraph(c);
    }

    static bool IsControl(char c) {
        return iscntrl(c);
    }
};

struct StringCharOperations {
    static int GetLength(std::string str) {
        return str.length();
    }

    static bool IsNullOrEmpty(std::string str) {
        return str.empty();
    }

    static std::string ToUpper(std::string str) {
        std::transform(str.begin(), str.end(), str.begin(), ::toupper);
        return str;
    }

    static std::string ToLower(std::string str) {
        std::transform(str.begin(), str.end(), str.begin(), ::tolower);
        return str;
    }

    static bool Contains(std::string str, std::string value) {
        return str.find(value) != std::string::npos;
    }

    static std::string Replace(std::string str, std::string oldValue, std::string newValue) {
        size_t pos = 0;
        while ((pos = str.find(oldValue, pos)) != std::string::npos) {
            str.replace(pos, oldValue.length(), newValue);
            pos += newValue.length();
        }
        return str;
    }

    static bool IsLetter(char c) {
        return CharOperations::IsLetter(c);
    }

    static bool IsDigit(char c) {
        return CharOperations::IsDigit(c);
    }

    static bool IsWhiteSpace(char c) {
        return CharOperations::IsWhiteSpace(c);
    }

    static bool IsPunctuation(char c) {
        return CharOperations::IsPunctuation(c);
    }

    static bool IsSymbol(char c) {
        return CharOperations::IsSymbol(c);
    }

    static bool IsControl(char c) {
        return CharOperations::IsControl(c);
    }
};

int main() {
    const int SIZE = 10000000;
    auto start = std::chrono::high_resolution_clock::now();

    double a = 122.0;
    double b = 33.0;
    double result = 0.0;

    //Counting time of arithmetic operations
    for (int i = 0; i < SIZE; i++) {
        result = Arithmetic::Multiply(a, b);
        result = Arithmetic::Subtract(a, b);
        result = Arithmetic::Divide(a, b);
    }

    auto end = std::chrono::high_resolution_clock::now();
    double duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
    std::cout << "Time taken to perform 10 million arithmetic operations = " << duration / 1000000 << " s." << std::endl;

    std::string s = "hello";
    StringCharOperations StrChOperations;

    //Counting time of string operations
    start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < SIZE; i++)
    {
        StrChOperations.ToLower(s);
        StrChOperations.IsDigit(s[0]);
        StrChOperations.GetLength(s);
    }
    end = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
    std::cout << "Time taken to perform 10 million char/string operations = " << duration / 1000000 << " s." << std::endl;
    return 0;
}
    
