﻿

using System.Diagnostics;

namespace Program
{
    struct Arithmetic
    {
        public static double Multiply(double a, double b)
        {
            return a * b;
        }

        public static double Subtract(double a, double b)
        {
            return a - b;
        }

        public static double Divide(double a, double b)
        {
            if (b == 0)
            {
                throw new DivideByZeroException("Cannot divide by zero");
            }
            return a / b;
        }

    }

    interface ICharOperations
    {
        bool IsLetter(char c);
        bool IsDigit(char c);
        bool IsWhiteSpace(char c);
        bool IsPunctuation(char c);
        bool IsSymbol(char c);
        bool IsControl(char c);
    }

    interface IStringCharOperations : ICharOperations
    {
        int GetLength(string str);
        bool IsNullOrEmpty(string str);
        string ToUpper(string str);
        string ToLower(string str);
        bool Contains(string str, string value);
        string Replace(string str, string oldValue, string newValue);
    }

    struct CharOperations : ICharOperations
    {
        public bool IsLetter(char c)
        {
            return char.IsLetter(c);
        }

        public bool IsDigit(char c)
        {
            return char.IsDigit(c);
        }

        public bool IsWhiteSpace(char c)
        {
            return char.IsWhiteSpace(c);
        }

        public bool IsPunctuation(char c)
        {
            return char.IsPunctuation(c);
        }

        public bool IsSymbol(char c)
        {
            return char.IsSymbol(c);
        }

        public bool IsControl(char c)
        {
            return char.IsControl(c);
        }
    }

    struct StringCharOperations : IStringCharOperations
    {
        public int GetLength(string str)
        {
            return str.Length;
        }

        public bool IsNullOrEmpty(string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public string ToUpper(string str)
        {
            return str.ToUpper();
        }

        public string ToLower(string str)
        {
            return str.ToLower();
        }

        public bool Contains(string str, string value)
        {
            return str.Contains(value);
        }

        public string Replace(string str, string oldValue, string newValue)
        {
            return str.Replace(oldValue, newValue);
        }

        public bool IsLetter(char c)
        {
            return char.IsLetter(c);
        }

        public bool IsDigit(char c)
        {
            return char.IsDigit(c);
        }

        public bool IsWhiteSpace(char c)
        {
            return char.IsWhiteSpace(c);
        }

        public bool IsPunctuation(char c)
        {
            return char.IsPunctuation(c);
        }

        public bool IsSymbol(char c)
        {
            return char.IsSymbol(c);
        }

        public bool IsControl(char c)
        {
            return char.IsControl(c);
        }
    }

    class Program
    {
        static void Main()
        {
            const int SIZE = 10000000;
            Stopwatch stopwatch = new Stopwatch();
            Arithmetic[] array = new Arithmetic[SIZE];

            double a = 122.0;
            double b = 33.0;
            double result = 0.0;

            //Counting time of arithmetic operations
            stopwatch.Start();

            for (int i = 0; i < SIZE; i++)
            {
                result = Arithmetic.Multiply(a, b);
                result = Arithmetic.Subtract(a, b);
                result = Arithmetic.Divide(a, b);
            }

            stopwatch.Stop();

            TimeSpan ts = stopwatch.Elapsed;
            Console.WriteLine($"Time taken to perform 10 million arithmetic operations = {ts.TotalSeconds} s.");

            string s = "hello";
            StringCharOperations StrChOperations = new StringCharOperations();

            //Counting time of string operations
            stopwatch.Restart();
            for (int i = 0; i < SIZE; i++)
            {
                StrChOperations.ToLower(s);
                StrChOperations.IsDigit(s[0]);
                StrChOperations.GetLength(s);
            }
            stopwatch.Stop();

            ts = stopwatch.Elapsed;
            Console.WriteLine($"Time taken to perform 10 million char/string operations = {ts.TotalSeconds} s.");
        }
    }
}