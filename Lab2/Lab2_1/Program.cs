﻿

using System.Diagnostics;

namespace Program
{

    public class Person
    {
        public Person(string name, int age, double hight)
        {
            Name = name;
            Age = age;
            Hight = hight;
        }
        private string Name { get; set; }
        private int Age { get; set; }
        private double Hight { get; set; }

        public override bool Equals(object? obj) => obj is Person &&
            obj.GetHashCode() == HashCode.Combine(Name, Age, Hight);
        public override int GetHashCode() => HashCode.Combine(Name, Age, Hight);
        public override String ToString() => Name + $"({Age} y. o)";

        public Person Clone()
        {
            return (Person)MemberwiseClone();
        }
    }


    class Arithmetic
    {
        public static double Multiply(double a, double b)
        {
            return a * b;
        }

        public static double Subtract(double a, double b)
        {
            return a - b;
        }

        public static double Divide(double a, double b)
        {
            if (b == 0)
            {
                throw new DivideByZeroException("Cannot divide by zero");
            }
            return a / b;
        }


    }

    public class CharOperations
    {
        public static bool IsLetter(char c)
        {
            return char.IsLetter(c);
        }

        public static bool IsDigit(char c)
        {
            return char.IsDigit(c);
        }

        public static bool IsWhiteSpace(char c)
        {
            return char.IsWhiteSpace(c);
        }

        public static bool IsPunctuation(char c)
        {
            return char.IsPunctuation(c);
        }

        public static bool IsSymbol(char c)
        {
            return char.IsSymbol(c);
        }

        public static bool IsControl(char c)
        {
            return char.IsControl(c);
        }
    }

    public class StringCharOperations : CharOperations
    {
        public static int GetLength(string str)
        {
            return str.Length;
        }

        public static bool IsNullOrEmpty(string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static string ToUpper(string str)
        {
            return str.ToUpper();
        }

        public static string ToLower(string str)
        {
            return str.ToLower();
        }

        public static bool Contains(string str, string value)
        {
            return str.Contains(value);
        }

        public static string Replace(string str, string oldValue, string newValue)
        {
            return str.Replace(oldValue, newValue);
        }
    }

    class Program
    {
        static void Main()
        {
            const int SIZE = 10000000;
            Stopwatch stopwatch = new Stopwatch();
            Arithmetic[] array = new Arithmetic[SIZE];

            double a = 2.0;
            double b = 3.0;
            double result = 0.0;

            //Counting time of arithmetic operations
            stopwatch.Start();

            for (int i = 0; i < SIZE; i++)
            {
                result = Arithmetic.Multiply(a, b);
                result = Arithmetic.Subtract(a, b);
                result = Arithmetic.Divide(a, b);
            }

            stopwatch.Stop();

            TimeSpan ts = stopwatch.Elapsed;
            Console.WriteLine($"Time taken to perform 10 million arithmetic operations = {ts.TotalSeconds} s.");

            string s = "hello";

            //Counting time of string operations
            stopwatch.Restart();
            for (int i = 0; i < SIZE; i++)
            {
                StringCharOperations.ToLower(s);
                StringCharOperations.IsDigit(s[0]);
                StringCharOperations.GetLength(s);
            }
            stopwatch.Stop();

            ts = stopwatch.Elapsed;
            Console.WriteLine($"Time taken to perform 10 million char/string operations = {ts.TotalSeconds} s.");
        }
    }
}